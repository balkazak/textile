$(function() {


    $('section').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $('section').addClass('delay fadeOutRight');
    });



    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true,
        mainClass: 'mfp-fade'
    });

    $(document).ready(function(){

        var list = $(".list .card");
        var numToShow = 12;
        var button = $("#next");
        var numInList = list.length;
        list.hide();
        if (numInList > numToShow) {
            button.show();
        }
        list.slice(0, numToShow).show();

        button.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + 4).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
                button.hide();
            }
        });

    });


	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});


	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "/mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Спасибо!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
    $('a[href*="#callback"], a[href*="#test-popup"]').magnificPopup({
        type: 'inline',
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true,
        closeBtnInside: true,
        preloader: false,

    });

});
$(document).ready(function(){
    if (window.matchMedia('(max-width: 768px)').matches) {
        $("#category").hide();
    }
    $("#show").click(function(){
        $("#category").toggle(500);
    });
});


// Функция ymaps.ready() будет вызвана, когда
// загрузятся все компоненты API, а также когда будет готово DOM-дерево.
ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [43.252372, 76.905203],
            zoom: 15
        }, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [43.252372, 76.905203]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: 'TEXTILE'
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.
            draggable: true
        });

    myMap.geoObjects
        .add(myGeoObject)
        .add(myPieChart)
        .add(new ymaps.Placemark([43.252372, 76.905203], {
            balloonContent: 'цвет <strong>воды пляжа бонди</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))

}

var swiper1 = new Swiper('.swiper-1', {
    slidesPerView: 4,
    spaceBetween: 30,
    slidesPerGroup: 3,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.next-1',
        prevEl: '.prev-1',
    },
    breakpoints: {
        1200: {
            slidesPerView: 3,
            spaceBetweenSlides: 10,
        },
        992: {
            slidesPerView: 2,
            spaceBetweenSlides: 10,
        },
        768: {
            slidesPerView: 1,
            spaceBetweenSlides: 10,
        },
    }

});

var swiper2 = new Swiper('.swiper-2', {
    slidesPerView: 6,
    loop: true,
    navigation: {
        nextEl: '.next-2',
        prevEl: '.prev-2',
    },
    breakpoints: {
        1200: {
            slidesPerView: 5,
            spaceBetweenSlides: 10,
        },
        1000: {
            slidesPerView: 3,
            spaceBetweenSlides: 10,
        },
        768: {
            slidesPerView: 2,
            spaceBetweenSlides: 10,
        },
    }
});

var swiper3 = new Swiper('.swiper-3', {
    slidesPerView: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.next-3',
        prevEl: '.prev-3',
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});

var swiper4 = new Swiper('.swiper-4', {
    slidesPerView: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.next-4',
        prevEl: '.prev-4',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
    },
});

var swiper5 = new Swiper('.swiper-5', {
    slidesPerView: 'auto',
    loop: true,
    navigation: {
        nextEl: '.next-5',
        prevEl: '.prev-5',
    },
});
